package com.sheepit.client.hardware.gpu.nvidia;

import com.sun.jna.Library;
import com.sun.jna.ptr.IntByReference;

//https://docs.nvidia.com/deploy/nvml-api/group__nvmlSystemQueries.html#group__nvmlSystemQueries

public interface NVML extends Library {
	
	public int nvmlInit_v2();
	public int nvmlShutdown ();
	public int nvmlSystemGetDriverVersion(byte[] version, int length);

}
